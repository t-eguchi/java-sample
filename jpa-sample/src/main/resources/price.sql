create table price (
	base_date text not null,
	issue_code text not null,
	open real,
	high real,
	low real,
	close real,
	primary key (base_date, issue_code)
);

insert into price values ('20170712', '3222', 1117, 1120, 1103, 1111);
insert into price values ('20170713', '3222', 1118, 1118, 1104, 1107);