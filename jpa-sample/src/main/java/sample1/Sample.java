package sample1;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Sample {

	public static void main(String[] args) {
		EntityManagerFactory fac = Persistence.createEntityManagerFactory("test");
		EntityManager em = fac.createEntityManager();
		List<Price> prices = em.createQuery("SELECT p FROM Price p", Price.class).getResultList();
		prices.forEach(price -> {
			System.out.println(price);
		});
	}

}
