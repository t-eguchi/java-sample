package sample1;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
public class Price {

	@Id
	@Column(name = "base_date")
	private String baseDate;

	@Id
	@Column(name = "issue_code")
	private String issueCode;

	@Column
	private BigDecimal open;

	@Column
	private BigDecimal high;

	@Column
	private BigDecimal low;

	@Column
	private BigDecimal close;
}
